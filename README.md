# pydantic-factur-x

The main feature of this Python library is to generate Factur-X and Order-X xml files.
It uses pydantic to make more easy, accessible and idiomatic the manipulation of all the elements defined in Factur-X and order-X.
No need to hassle with xml, only use pydantic object models.

## What is factur-X ?

Factur-X is a Franco-German standard for hybrid e-invoice (PDF for users and XML data for process automation), the first implementation of the European Semantic Standard EN 16931 published by the European Commission on October 16th 2017. Factur-X is the same standard than ZUGFeRD 2.2.

Factur-X is at the same time a full readable invoice in a PDF A/3 format, containing all information useful for its treatment, especially in case of discrepancy or absence of automatic matching with orders and / or receptions, and a set of invoice data presented in an XML structured file conformant to EN16931 (syntax CII D16B), complete or not, allowing invoice process automation.

The first objective of Factur-X is to enable suppliers, invoice issuers, to create added-value e-invoices, containing a maximum of information in structured form, according to their ability to produce them in this form, and to let customers recipients free to use the invoice data and / or the readable presentation, depending on their needs and their invoice process maturity on automation.

### order-x

Order-X is the implementation of Factur-X for purchase orders.

## Installation

(tbd)

## Usage

(tbd)

## License

This library is published under the MIT licence


